import React, { Component } from "react";
import Particles from "react-particles-js";

class Canvas extends Component {

  state = {
    width: "0px",
    height: "0px"
  };

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }
  updateWindowDimensions = () => {
    this.setState({
      width: `${window.innerWidth}px`,
      height: `${window.innerHeight}px`
    });
  };

  render() {
    const { width, height } = this.state;
    console.log(width, height);

    return (
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0
        }}
      >
        <Particles
          {...this.state}
          params={{
            "particles": {
              "number": {
                  "value": 50
              },
              "size": {
                  "value": 3
            }},
            "interactivity": {
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "repulse"
                    }
                }
            }
          }}
        />
      </div>
    );
  }
}

export default Canvas;
