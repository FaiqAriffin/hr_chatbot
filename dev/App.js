import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Widget,
  addResponseMessage,
  addUserMessage,
  renderCustomComponent,
  toggleInputDisabled,
  toggleMsgLoader,
  setQuickButtons
} from '../index';
import kwsp_logo from "../assets/kwsp_logo.png";
import bot_logo from "../assets/bot_pic.png";
import { ApiAiClient } from "api-ai-javascript";
import turnDown from "turndown";


const client = new ApiAiClient({
  accessToken: "80160483c1c44c70b89fbb9234bbfbc4"
});

// html to markdown service
var turndownService = new turnDown();

export default class App extends Component {

  componentDidMount() {

  toggleMsgLoader();

    setTimeout(() => {
      toggleMsgLoader();
      addResponseMessage("Hi! Saya Muin, chatbot PMD. ");

      toggleMsgLoader();
      setTimeout(() => {
        toggleMsgLoader();
        addResponseMessage(
          "Saya harap saya dapat membantu pertanyaan anda."
        );

      }, 2000);

    }, 3000);

  }

  handleNewUserMessage = newMessage => {
    toggleMsgLoader();
    // newMessage processed by DialogFlow
    client.textRequest(newMessage).then(function(response) {
      var result;
      try {
        result = response.result.fulfillment.speech;
      } catch (error) {
        result = "";
      }

      // convert result from html to markdown
      var markdown = turndownService.turndown(result);

        setTimeout(() => {
          toggleMsgLoader();

          addResponseMessage(markdown);

        }, 1000);
    });
  };

  render() {
    return (
      <div>
        <Widget
          titleAvatar={kwsp_logo}
          profileAvatar={bot_logo}
          title=""
          subtitle="People Matters Department"
          handleNewUserMessage={this.handleNewUserMessage}
          handleQuickButtonClicked={this.handleQuickButtonClicked}
          toggleWidget="true"
        />
        <div>{this.displayMessage}</div>
      </div>
    );
  }

}
