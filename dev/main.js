import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Particles from "./Particles";

import Widget from './App.js';
import './App.css';
import Logo from '../assets/kwsp_logo.png';

class BigDaddy extends Component{

  render(){
    return(
      <div>
        <Particles className="particles"/>
        <Widget />
        <img src={Logo} align="bottom" className="epf-logo"/>
        <p className="h1">Human Resource Chatbot</p>
        <p className="h2">by People Matters Department</p>
      </div>
    )
  }

}

ReactDOM.render(<BigDaddy />, document.getElementById('root'));
